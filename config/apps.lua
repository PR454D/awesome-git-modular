local _M = {
    terminal = "kitty ",
    floating_terminal = "kitty --class floating-term",
    scratchpad = "kitty --class scratchpad -e notetaker",
    launcher = 'rofi -show combi',
    editor   = os.getenv('EDITOR')   or 'nvim',
}

_M.editor_cmd = _M.terminal .. ' -e ' .. _M.editor
_M.manual_cmd = _M.terminal .. ' -e man awesome'

return _M
