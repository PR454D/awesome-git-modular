local awful = require'awful'
local gears = require'gears'
-- awful.spawn.single_instance("firefox", awful.rules.rules)
do
  local autostarts =
  {
    "kdeconnect-indicator",
    "picom",
    "emacs --daemon",
    "lxpolkit &",
    "nm-applet &",
		"cbatticon"
  }

  for _,i in pairs(autostarts) do
    awful.spawn.easy_async_with_shell(
      'ps -C '.. i ..' |wc -l',
      function(stdout, stderr, reason, exit_code)
        gears.debug.dump(stdout)
        if tonumber(stdout) or 0 < 2 then
          awful.spawn.single_instance(i)
        end
      end
    )
  end
end
