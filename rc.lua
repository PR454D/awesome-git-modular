-- awesome_mode: api-level=4:screen=on

-- load luarocks if installed
pcall(require, 'luarocks.loader')

-- load theme
local beautiful = require'beautiful'
local gears = require'gears'
beautiful.init(gears.filesystem.get_configuration_dir() .. 'themes/theme.lua')

-- load key and mouse bindings
require'bindings'

-- load rules
require'rules'

-- load signals
require'signals'

--autostart apps
require'autostart'
local status_ok, modules = pcall(require, "modules")

if not status_ok then
  print("Hello World")
else
  print("Done")
end
