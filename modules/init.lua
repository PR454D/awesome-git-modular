local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")
local lain = require("lain")
local freedesktop = require("freedesktop")
require("collision")()
local machi = require("layout-machi")

markup = lain.util.markup
space2 = markup.font("Fantasque Sans Mono 10", " ")
local fsroothome = lain.widget.fs({
	settings = function ()
		widget:set_text("/home" .. fs_now["/home"].percentage .. "%")
	end
})


local mynetdown = wibox.widget.textbox()
local mynetup = lain.widget.net {
	settings = function()
		widget:set_markup(markup("#eaffdd", "up:"..net_now.sent .."↑"))
	end
}
